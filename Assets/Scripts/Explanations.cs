﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explanations : MonoBehaviour
{
    private Animator explanationsAnimator;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void OnDestroy()
    {
        GameObject explanations = GameObject.FindWithTag("Explanations");
        if (explanations != null)
        {
            explanationsAnimator = explanations.GetComponent<Animator>();
            if (explanationsAnimator != null)
                explanationsAnimator.SetBool("IsShow", false);
        }
    }
}
