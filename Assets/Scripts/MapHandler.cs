﻿using System.CodeDom;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using UnityEngine;
using UnityEngine.Tilemaps;

public class MapHandler : MonoBehaviour
{
    public Tilemap tilemap;
    DialogueTrigger dialogueTrigger;
    public Sprite defaultSprite;
    public int squareSize;
    public GameObject nextMap;
    public GameObject winMap;
    private char[,] map;
    private bool finished = false;

    void Start()
    {
        dialogueTrigger = GetComponentInChildren<DialogueTrigger>();
    }

    public int GetBigGridSize()
    {
        return ((int)Mathf.Sqrt(squareSize));
    }

    public void Fill()
    {
        //tilemap = gameObject.GetComponent(typeof(Tilemap)) as Tilemap;
        Tile tile = ScriptableObject.CreateInstance<Tile>();

        for (int x = 0; x < squareSize; x++)
        {
            for (int y = 0; y < squareSize; y++)
            {
                tile = ScriptableObject.CreateInstance<Tile>();
                tile.sprite = defaultSprite;
                tilemap.SetTile(new Vector3Int(x, y, 0), tile);
            }
        }
    }

    public void Parse()
    {
        if (!tilemap)
        {
            finished = true;
        }
        map = new char[squareSize, squareSize];
        for (int x = 0; x < squareSize; x++)
        {
            for (int y = 0; y < squareSize; y++)
            {
                Sprite sprite = tilemap.GetSprite(new Vector3Int(x, y, 0));
                if (sprite)
                {
                    map[x, y] = sprite.name[0];
                }
                else
                    map[x, y] = 'G';
            }
        }
    }

    public GameObject GetNextMap()
    {
        if (!finished)
            return (nextMap);
        return (winMap);
    }

    public bool IsFinished()
    {
        return finished;
    }

    public Vector3 GetStartingPoint()
    {
        Vector3Int startingPosition = Vector3Int.zero;

        for (int y = squareSize - 1; y >= 0; y--)
        {
            for (int x = 0; x < squareSize; x++)
            {
                if (map[x, y] == 'S')
                {
                    startingPosition = new Vector3Int(x, y, 0);
                    break;
                }
            }
        }
        return (tilemap.layoutGrid.CellToWorld(startingPosition));
    }

    public Vector3Int GetAfterZoomPosition(Vector3 position)
    {
        Vector3Int bigCellPos = WorldToBigCell(position);
        Vector3Int cellPos = WorldToLittleCell(position);
        Vector3Int newPos = new Vector3Int((cellPos.x % GetBigGridSize()) * GetBigGridSize() + bigCellPos.x, (cellPos.y % GetBigGridSize()) * GetBigGridSize() + bigCellPos.y, 0);
        return newPos;
    }

    public Vector3 LittleCellToWorld(Vector3Int position)
    {
        return (tilemap.layoutGrid.CellToWorld(position));
    }

    public Vector3Int WorldToLittleCell(Vector3 position)
    {
        return (tilemap.layoutGrid.WorldToCell(position));
    }

    public Vector3Int WorldToBigCell(Vector3 position)
    {
        Vector3Int cellPos = WorldToLittleCell(position);
        Vector3Int bigCellPos = new Vector3Int(cellPos.x / GetBigGridSize(), cellPos.y / GetBigGridSize(), 0);
        return (bigCellPos);
    }

    public Vector3 GetWorldBigCellPosition(Vector3 position)
    {
        Vector3Int bigCellPos = WorldToBigCell(position) * GetBigGridSize();
        return LittleCellToWorld(bigCellPos);
    }

    public bool IsWalkable(Vector3Int cellToCheck)
    {
        if (!tilemap)
            return (false);
        if (cellToCheck.x >= 0 && cellToCheck.x < squareSize && cellToCheck.y >= 0 && cellToCheck.y < squareSize && map[cellToCheck.x, cellToCheck.y] != 'W')
        {
            return (true);
        }
        return (false);
    }

    public void OnPosition(Vector3 position)
    {
        Vector3Int cellPos = WorldToLittleCell(position);
        if (!finished && cellPos.x >= 0 && cellPos.x < squareSize && cellPos.y >= 0 && cellPos.y < squareSize && map[cellPos.x, cellPos.y] == 'O')
        {
            dialogueTrigger.OnTriggerExitDialogue();
            finished = true;
        }
    }

    public void printMap()
    {

        for (int y = squareSize - 1; y >= 0; y--)
        {
            string line = string.Empty;

            for (int x = 0; x < squareSize; x++)
            {
                line = line + map[x, y];
            }
            Debug.Log(line);
        }
    }
}

