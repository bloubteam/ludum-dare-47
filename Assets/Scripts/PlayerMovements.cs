﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using UnityEngine;
using UnityEngine.InputSystem;
using Random = UnityEngine.Random;

public class PlayerMovements : MonoBehaviour
{
    [SerializeField] ParticleSystem _system;

    private Vector2 currentDisplacement;
    private DialogueManager _dialogueManager;
    private bool isMoving;
    private Vector2 destination;
    private Vector2 origin;
    private float timeElapsedForMovement;
    private MapHandler mapHandler;
    public float timeForDisplacement = 1f;
    private GameManager gameManager;
    private SpriteRenderer playerSprite;
    private Animator playerAnimator;
    private AudioSource walkSound;
    private bool playerLeftFoot = true;
    public AudioClip[] walkingSoundClip;

    public bool shouldMove = false;

    void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
        currentDisplacement = Vector2.zero;
        isMoving = false;
        _dialogueManager = FindObjectOfType<DialogueManager>();
        playerSprite = GetComponentInChildren<SpriteRenderer>();
        playerAnimator = GetComponentInChildren<Animator>();
        walkSound = GetComponent<AudioSource>();
        playerAnimator.speed = 0.5f / timeForDisplacement;
    }


    void Update()
    {
        float progression;

        if (shouldMove == false || _dialogueManager.InDialog)
        {
            isMoving = false;
            timeElapsedForMovement = 0f;
            currentDisplacement = Vector2.zero;
        }
        if (isMoving == true)
        {
            timeElapsedForMovement += Time.deltaTime;
            progression = Mathf.Clamp(timeElapsedForMovement / timeForDisplacement, 0f, 1f);
            this.transform.position = Vector3.Lerp(mapHandler.LittleCellToWorld(V2ToV3t(origin)), mapHandler.LittleCellToWorld(V2ToV3t(destination)), progression);
            if (progression == 1f)
            {
                mapHandler.OnPosition(this.transform.position);
                if (mapHandler.IsFinished())
                {
                    _system.transform.position = Vector3.right * 10000f;
                }
                else
                {
                    _system.transform.position = mapHandler.LittleCellToWorld(mapHandler.GetAfterZoomPosition(transform.position)) + 0.5f * Vector3.up + 0.5f * Vector3.right;
                }
                isMoving = false;
            }
        }
        else if (currentDisplacement != Vector2.zero)
        {
            float angleToRotate = 0f;

            origin = V3tToV2(mapHandler.WorldToLittleCell(this.transform.position));
            Vector2 newDestination = origin + currentDisplacement;
            if (currentDisplacement == Vector2.right)
                angleToRotate = 90f;
            else if (currentDisplacement == Vector2.up)
                angleToRotate = 180f;
            else if (currentDisplacement == Vector2.left)
                angleToRotate = -90f;

            playerSprite.transform.RotateAround(playerSprite.transform.position, Vector3.forward, angleToRotate - playerSprite.transform.eulerAngles.z);
            if (mapHandler.IsWalkable(V2ToV3t(newDestination)))
            {
                if (playerLeftFoot == true)
                    playerAnimator.Play("Base Layer.playerLeftFoot");
                else
                    playerAnimator.Play("Base Layer.PlayerRightFoot");
                walkSound.clip = walkingSoundClip[Random.Range(0, walkingSoundClip.Length)];
                walkSound.Play();
                playerLeftFoot = !(playerLeftFoot);
                isMoving = true;
                destination = newDestination;
                timeElapsedForMovement = 0f;
            }
        }
        
    }

    void OnMove(InputValue value)
    {
        if (shouldMove || !_dialogueManager.InDialog)
        {
            Vector2 newDisplacement = value.Get<Vector2>();

            if (newDisplacement == Vector2.zero)
            {
                currentDisplacement = Vector2.zero;
            }
            else if (Mathf.Abs(newDisplacement.x) > Mathf.Abs(currentDisplacement.x))
            {
                currentDisplacement = Mathf.Sign(newDisplacement.x) * Vector2.right;
            }
            else if (Mathf.Abs(newDisplacement.y) > Mathf.Abs(currentDisplacement.y))
            {
                currentDisplacement = Mathf.Sign(newDisplacement.y) * Vector2.up;
            }
        }
        else
            currentDisplacement = Vector2.zero;
    }

    void OnZoom()
    {
        if (!isMoving && shouldMove && !_dialogueManager.InDialog)
        {
            gameManager.Zoom();
        }
    }

    private Vector2 V3tToV2(Vector3Int toConvert)
    {
        Vector2 result = Vector2.zero;

        result.x = (float)toConvert.x;
        result.y = (float)toConvert.y;
        return (result);
    }

    private Vector3Int V2ToV3t(Vector2 toConvert)
    {
        Vector3Int result = Vector3Int.zero;

        result.x = (int)toConvert.x;
        result.y = (int)toConvert.y;
        return (result);
    }

    public void SetMapHandler(MapHandler newMapHandler)
    {
        mapHandler = newMapHandler;
    }
}
