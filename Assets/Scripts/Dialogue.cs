﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct lineOfDialog
{
    public string speaker;
    public int objectToShow;
    [TextArea(3, 10)]
    public string line;
}

[System.Serializable]
public class Dialogue
{
    public lineOfDialog[] dialogs;
}
