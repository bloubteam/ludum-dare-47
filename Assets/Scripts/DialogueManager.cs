﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using TMPro;
using Debug = UnityEngine.Debug;

public class DialogueManager : MonoBehaviour
{
    public float delay = 0.1f;
    public bool allowDialogue = true;
    public Image[] objects;
    private Queue<lineOfDialog> queueDial;
    private Image[] characterSprite;
    private Image textFieldRenderer;
    private int numberOfCharacter;
    private bool inDialogue = false;
    private TextMeshProUGUI printedText;
    private bool fadeIn;
    private bool fadeOut;
    private float elapsedTime;
    private PlayerMovements _playerMovements;

    public bool InDialog => inDialogue;

    public void Start()
    {
        GameObject protagoniste = GameObject.FindWithTag("ProtagonistePortrait");
        GameObject laMort = GameObject.FindWithTag("LaMortPortrait");
        GameObject textField = GameObject.FindWithTag("TextField");
        GameObject player = GameObject.FindWithTag("Player");

        _playerMovements = FindObjectOfType<PlayerMovements>();

        numberOfCharacter = 2;
        characterSprite = new Image[numberOfCharacter];
        if (protagoniste == null || textField == null || laMort == null || player == null)
        {
            Debug.Log("Could not find sprite for dialogue, it sucks but I decided to not allow dialogue");
            allowDialogue = false;
        }
        else
        {
            characterSprite[0] = protagoniste.GetComponent<Image>();
            characterSprite[1] = laMort.GetComponent<Image>();
            textFieldRenderer = textField.GetComponent<Image>();
            printedText = this.GetComponentInChildren<TextMeshProUGUI>();

            foreach (Image objectShowable in objects)
            {
                if (objectShowable != null)
                    objectShowable.gameObject.SetActive(false);
            }
            foreach (Image rende in characterSprite)
            {
                rende.gameObject.SetActive(false);
            }
            textFieldRenderer.gameObject.SetActive(false);
            queueDial = new Queue<lineOfDialog>();
        }

    }

    void Update()
    {
        elapsedTime += Time.deltaTime;
    }

    public void StartDialogue(Dialogue dialogueToLoad, bool tfadeIn, bool tfadeOut)
    {
        if (allowDialogue == true)
        {
            elapsedTime = 0;
            fadeIn = tfadeIn;
            fadeOut = tfadeOut;
            lineOfDialog[] arrayOfDial = dialogueToLoad.dialogs;

            inDialogue = true;
            textFieldRenderer.gameObject.SetActive(true);
            _playerMovements.shouldMove = false;
            foreach (lineOfDialog dial in arrayOfDial)
            {
                queueDial.Enqueue(dial);
            }
            TriggerNextLine();
        }
    }

    public void OnClick()
    {
        if (inDialogue == true && elapsedTime > delay)
        {
            TriggerNextLine();
        }
    }

    public void TriggerNextLine()
    {
        foreach (Image objectShowable in objects)
        {
            if (objectShowable != null)
                objectShowable.gameObject.SetActive(false);
        }
        foreach (Image rende in characterSprite)
        {
            rende.gameObject.SetActive(false);
        }
        if (queueDial.Count == 0)
        {
            inDialogue = false;
            textFieldRenderer.gameObject.SetActive(false);
            printedText.text = "";
            GameManager gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
            if (fadeIn)
            {
                gameManager.managerSound.clip = gameManager.zoomClip[2];
                gameManager.managerZic.Stop();
                gameManager.managerSound.Play();
                gameManager.DoFadeIn();
            }
            else if (fadeOut)
            {
                gameManager.DoFadeOut();
                gameManager.managerZic.Play();
            }
            _playerMovements.shouldMove = true;
        }
        else
        {
            lineOfDialog dial = queueDial.Dequeue();
            int characterIndex = GetSpeakerIndex(dial.speaker);
            if (characterIndex >= numberOfCharacter)
                Debug.Log("Error: Unknown Character Name");
            else
                characterSprite[characterIndex].gameObject.SetActive(true);
            printedText.text = dial.line;
            Image objectToShow = objects[dial.objectToShow];
            if (objectToShow)
                objectToShow.gameObject.SetActive(true);
        }
    }

    private int GetSpeakerIndex(string speakerName)
    {
        int i = 0;

        while (i < numberOfCharacter && characterSprite[i].sprite.name != speakerName)
            i = i + 1;
        return (i);
    }
}
