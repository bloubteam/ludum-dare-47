﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueTrigger : MonoBehaviour
{
    public bool fadeIn;
    public bool fadeOut;
    public Dialogue entryDialogue;
    public Dialogue exitDialogue;
    private DialogueManager manager;

    public void Start()
    {
        manager = (DialogueManager)FindObjectOfType(typeof(DialogueManager));
    }

    public void OnTriggerEntryDialogue()
    {
        manager.StartDialogue(entryDialogue, false, fadeOut);
    }

    public void OnTriggerExitDialogue()
    {
        manager.StartDialogue(exitDialogue, fadeIn, false);
    }
}
