﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Collections.Specialized;
using System.Diagnostics;
using UnityEngine.InputSystem;
using UnityEngine;
using Debug = UnityEngine.Debug;

public class GameManager : MonoBehaviour
{
    [SerializeField] ParticleSystem _system;

    public GameObject startingMap;
    private bool noPlayer = false;
    private GameObject player;
    private GameObject map;
    private Camera worldCamera;
    private PlayerMovements playerMovements;
    private Animator darknessAnimator;

    //Music var
    public AudioSource managerSound;
    public AudioSource managerZic;
    public AudioClip[] zoomClip;

    // zoom vars
    public AnimationCurve curve;
    public float zoomTotalDuration = 1;
    private bool zoomInProgress = false;
    private float zoomDuration = 0;
    private GameObject oldMap;
    private bool initialDialogueStarted = false;
    private Vector3 oldPlayerPos;
    private Vector3 newPlayerPos;

    // Start is called before the first frame update
    void Start()
    {
        zoomInProgress = false;
        if (startingMap.scene.rootCount == 0)
        {
            map = Instantiate(startingMap, new Vector3(0, 0, 0), Quaternion.identity);
        }
        else
        {
            map = startingMap;
        }
        worldCamera = GameObject.FindWithTag("WorldCamera").GetComponent<Camera>();
        player = GameObject.FindWithTag("Player");
        darknessAnimator = GameObject.FindWithTag("Darkness").GetComponentInChildren<Animator>();
        AudioSource[] audioTab = GetComponents<AudioSource>();
        foreach (AudioSource source in audioTab)
        {
            if (source.clip.name == "music_ludum_dare")
                managerZic = source;
            else
                managerSound = source;
        }


        if (map == null || player == null)
        {
            noPlayer = true;
            Debug.Log("No map or player");
        }
        else
        {
            MapHandler mapHandler = map.GetComponent<MapHandler>();
            if (mapHandler)
            {
                Animator explanationsAnimator = GameObject.FindWithTag("Explanations").GetComponent<Animator>();
                explanationsAnimator.SetBool("IsShow", true);
                mapHandler = map.GetComponent<MapHandler>();
                playerMovements = player.GetComponent<PlayerMovements>();
                mapHandler.Parse();
                player.transform.position = mapHandler.GetStartingPoint();
                playerMovements.SetMapHandler(mapHandler);
                float mapSize = mapHandler.squareSize;
                worldCamera.transform.position = new Vector3(map.transform.position.x + mapSize / 2f, map.transform.position.y + mapSize / 2f, -10);
                worldCamera.orthographicSize = mapSize / 2f;
            }
            else
            {
                Debug.Log("No map handler in map");
            }
        }
        Debug.Log("START");
    }

    // Update is called once per frame
    void Update()
    {
        if (initialDialogueStarted == false && noPlayer == false)
        {
            DialogueTrigger dialogueTrigger = map.GetComponentInChildren<DialogueTrigger>();
            dialogueTrigger.OnTriggerEntryDialogue();
            initialDialogueStarted = true;
        }
        else if (zoomInProgress)
        {
            ZoomUpdate();
        }
    }

    public void DoFadeOut()
    {
        darknessAnimator.Play("Base Layer.fadeOut");
    }

    public void DoFadeIn()
    {
        darknessAnimator.Play("Base Layer.fadeIn");
    }

    void ZoomUpdate()
    {
        zoomDuration += Time.deltaTime;
        if (zoomDuration >= zoomTotalDuration)
        {
            zoomDuration = zoomTotalDuration;
            zoomInProgress = false;
        }

        float zoomProgress = zoomDuration / zoomTotalDuration;
        zoomProgress = curve.Evaluate(zoomProgress);

        foreach (Renderer _renderer in map.GetComponentsInChildren<Renderer>())
        {
            Color color = _renderer.material.color;
            _renderer.material.color = new Color(color.r, color.g, color.b, zoomProgress);
        }


        MapHandler mapHandler = map.GetComponent<MapHandler>();
        MapHandler oldMapHandler = oldMap.GetComponent<MapHandler>();

        // zoom camera to new position

        Vector3 newMapCenter = new Vector3(map.transform.position.x + (mapHandler.squareSize / 2f * map.transform.localScale.x), map.transform.position.y + (mapHandler.squareSize / 2f * map.transform.localScale.y), -10);
        Vector3 oldMapCenter = new Vector3(oldMap.transform.position.x + oldMapHandler.squareSize / 2f, oldMap.transform.position.y + oldMapHandler.squareSize / 2f, -10);
        Vector3 newCameraPosition = oldMapCenter + ((newMapCenter - oldMapCenter) * zoomProgress);
        worldCamera.transform.position = newCameraPosition;
        float newOrthoSize = mapHandler.squareSize / 2f * map.transform.localScale.x;
        float oldOrthoSize = oldMapHandler.squareSize / 2f;
        float orthoDiff = oldOrthoSize - newOrthoSize;
        worldCamera.orthographicSize = oldOrthoSize - orthoDiff * zoomProgress;

        // move player to new position

        player.transform.position = Vector3.Lerp(oldPlayerPos, newPlayerPos, zoomProgress);
        player.transform.localScale = Vector3.Lerp(Vector3.one, map.transform.localScale, zoomProgress);

        if (!zoomInProgress)
        {
            Vector3Int playerCellPos = mapHandler.WorldToLittleCell(newPlayerPos);
            map.transform.localScale = Vector3.one;
            map.transform.position = oldMap.transform.position;
            player.transform.localScale = Vector3.one;
            player.transform.position = mapHandler.LittleCellToWorld(playerCellPos);
            playerMovements.SetMapHandler(mapHandler);
            mapHandler.OnPosition(player.transform.position);
            worldCamera.transform.position = new Vector3(map.transform.position.x + mapHandler.squareSize / 2f, map.transform.position.y + mapHandler.squareSize / 2f, -10);
            worldCamera.orthographicSize = mapHandler.squareSize / 2f;
            if (oldMapHandler.IsFinished())
            {
                DialogueTrigger dialogueTrigger = map.GetComponentInChildren<DialogueTrigger>();
                dialogueTrigger.OnTriggerEntryDialogue();
            }
            else
                playerMovements.shouldMove = true;
            Destroy(oldMap);
            _system.Play();
            _system.transform.position = mapHandler.LittleCellToWorld(mapHandler.GetAfterZoomPosition(playerMovements.transform.position)) + 0.5f * Vector3.up + 0.5f * Vector3.right;

            //StartCoroutine(WaitBeforeParticle(mapHandler));

            UnityEngine.Debug.Log("Terminated zoom");
        }
    }



    public void Zoom()
    {
        MapHandler oldMapHandler = map.GetComponent<MapHandler>();
        GameObject nextMap = oldMapHandler.GetNextMap();

        if (managerSound.isPlaying == true)
            return;
        if (!oldMapHandler.GetNextMap())
            return;
        if (zoomInProgress)
        {
            return;
        }
        playerMovements.shouldMove = false;
        zoomDuration = 0;
        oldPlayerPos = player.transform.position;

        oldMap = map;
        map = Instantiate(oldMapHandler.GetNextMap(), oldMapHandler.GetWorldBigCellPosition(player.transform.position) + Vector3.back, Quaternion.identity);
        MapHandler mapHandler = map.GetComponent<MapHandler>();
        mapHandler.Parse();
        managerSound.clip = zoomClip[1];
        managerSound.Play();
        if (!oldMapHandler.IsFinished() && !mapHandler.IsWalkable(oldMapHandler.GetAfterZoomPosition(oldPlayerPos)))
        {
            UnityEngine.Debug.Log("Cant zoom here");
            managerSound.Stop();
            managerSound.clip = zoomClip[0];
            managerSound.Play();
            Destroy(map);
            map = oldMap;
            playerMovements.shouldMove = true;
            return;
        }
        _system.Stop();
        map.transform.localScale = new Vector3((float)oldMapHandler.GetBigGridSize() / mapHandler.squareSize, (float)oldMapHandler.GetBigGridSize() / mapHandler.squareSize, 1);


        if (oldMapHandler.IsFinished())
            newPlayerPos = mapHandler.GetStartingPoint();
        else
            newPlayerPos = mapHandler.LittleCellToWorld(oldMapHandler.GetAfterZoomPosition(oldPlayerPos));
        foreach (Renderer _renderer in map.GetComponentsInChildren<Renderer>())
        {
            Color color = _renderer.material.color;
            _renderer.material.color = new Color(color.r, color.g, color.b, 0);
        }
        zoomInProgress = true;
        oldMap.name = "OldMap";
        map.name = "Map";
    }
}
