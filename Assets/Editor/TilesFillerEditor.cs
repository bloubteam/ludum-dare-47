﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(MapHandler))]
public class MapHandlerEditor : Editor
{
    SerializedProperty mapHandler;

    void OnEnable()
    {
        mapHandler = serializedObject.FindProperty("mapHandler");
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        var t = (target as MapHandler);
        if (GUILayout.Button("Fill"))
        {
            t.Fill();
        }
        if (GUILayout.Button("Parse"))
        {
            t.Parse();
        }
        if (GUILayout.Button("printMap"))
        {
            t.printMap();
        }
    }
}